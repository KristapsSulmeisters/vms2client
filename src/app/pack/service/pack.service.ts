import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Pack} from "../Pack";

@Injectable({
  providedIn: 'root'
})
export class PackService {
  private baseLink = 'http://localhost:8090/packs';
  private itemsLink = 'http://localhost:8090/items';
  private usersLink = 'http://localhost:8090/users';
  private machinesLink = 'http://localhost:8090/machines';

  constructor( private http: HttpClient) { }

  getAllPacks() :Observable<any>{
    return this.http.get(`${this.baseLink}`);
  }

  getAllItems() {
    return this.http.get(`${this.itemsLink}`);

  }

  getAllUsers() {
    return this.http.get(`${this.usersLink}`);

  }

  getAllMachines() {
    return this.http.get(`${this.machinesLink}`);

  }

  deletePack(id:number ) :Observable<any>{
    return this.http.delete(`${this.baseLink}/${id}`);

  }
  getPackById(id:number):Observable<any>{
    return this.http.get(`${this.baseLink}/${id}`);
  }

  editPack(pack: Object, id):Observable<any>{
    return this.http.put(`${this.baseLink}/${id}`, pack);
  }

  createPack(pack: Object): Observable<Object> {
    console.log(this.http.post(`${this.baseLink}`, pack))
    return this.http.post(`${this.baseLink}`, pack);
  }

}
