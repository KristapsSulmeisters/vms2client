import { Injectable } from '@angular/core';
import {User} from "../user";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UsersvcService {

  private baseLink = 'http://localhost:8090/users'
  private userDetailsLink = 'http://localhost:8090/userdetails'
  constructor(private http: HttpClient) { }

  getUsrList(): Observable<any> {
    return this.http.get(`${this.baseLink}` );
  }

  deleteUsr(id: number): Observable<any>  {
    return this.http.delete(`${this.baseLink}/${id}`, { responseType: 'text' });
  }

  getUser(id: number): Observable<any> {
    return this.http.get(`${this.userDetailsLink}/${id}`);
  }

  updateUser(id: number, value: any):Observable<Object> {
    return this.http.put(`${this.baseLink}/${id}`, value);
  }
}
